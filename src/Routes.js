import Home from "./Pages/Home";
import MovieDetails from "./Pages/MovieDetails";

export const publicRoutes = [
  {
    component: Home,
    path: "/",
    exact: true,
  },
  {
    component: MovieDetails,
    path: "/movie-detail/:id",
  },
];
