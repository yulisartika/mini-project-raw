import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import { publicRoutes } from "./Routes";

function App() {
  return (
    <Router>
      <Switch>
        {publicRoutes.map((route, index) => (
          <Route
            component={route.component}
            path={route.path}
            exact={route.exact}
            key={index}
          />
        ))}
      </Switch>
    </Router>
  );
}

export default App;
