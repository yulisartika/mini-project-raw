import { combineReducers } from "redux";

import MovieLists from "./MovieLists";

export default combineReducers({ MovieLists });
